import { Injectable } from '@angular/core';
import { Events, LoadingController } from 'ionic-angular';
import { ItemRestProvider } from '../../providers/rest/itemrest';
import { ToastService } from './../../services/toast/toast.service';

@Injectable()
export class ItemService {
	constructor(		
		public events: Events,
		private loadingCtrl: LoadingController,
		private itemrest: ItemRestProvider,
		private toast: ToastService
	) {

	}
	
	validateImage(file) {
		let response = {
			success: true,
			errorMessage: ''
		};  
	  	if (file == undefined){
	  		return response;
	  	}  		  	
	  	if (file.type.indexOf('image')<0){
	  		response.errorMessage = 'Por favor selecciona una imágen válida';  	
	  		response.success = false;	
	  		return response;	  		
	  	}
	  	return response;	  	
  	}

  	validateFormItem(form){
  		let response = {
			success: true,
			errorMessage: ''
		};
  		if(!form.valid){
  			response.success = false;	        
	        let separator = '';
	        if (!form.controls.title.valid) {
	          response.errorMessage = 'Ingresa un título';          
	          separator = ', ';
	        }
	        if (!form.controls.description.valid) {
	          response.errorMessage = response.errorMessage + separator + 'Ingresa una descripción';
	        }
	        return response; 	        
	    }
	    return response;
  	}

  	itemValidations(selectedFile, form)
  	{	
  		let response = {
			success: true,
			errorMessage: ''
		};
  		response = this.validateImage(selectedFile);	    
	    if (!response.success){
	    	this.toast.show(response.errorMessage);
	      	return response;
	    }
	    response = this.validateFormItem(form);
	    if(!response.success){        
	        this.toast.show(response.errorMessage);         
	        return response;
	    }
	    return response;
  	}

  	sendItem(url, selectedFile, item){  		

  		let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
	    loading.present();

	    this.itemrest.sendItem(url, selectedFile, item).then(ref =>{
	  		loading.dismiss();
	  		this.toast.show('Compartido!');
	      this.events.publish('user:items', item, 'add', Date.now());
	      }).catch((ref) =>{	
	        loading.dismiss();      
	        console.log(ref.error.code);
	        if (ref.error.code == 1002 || ref.error.code==2002) {
	          this.toast.show('Tu sesión expiró, logueate de nuevo!');
	          this.events.publish('user:session', {}, Date.now());
	        } else {
	          this.toast.show('Error, intenta nuevamente!');
	        }		    
	   	});
  	}
}
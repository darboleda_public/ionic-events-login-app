import { Injectable } from '@angular/core';
import { Events, LoadingController } from 'ionic-angular';
import { MessageRestProvider } from '../../providers/rest/messagerest';
import { ToastService } from './../../services/toast/toast.service';

@Injectable()
export class MessageService {
	constructor(		
		public events: Events,
		private loadingCtrl: LoadingController,
		private messagerest: MessageRestProvider,
		private toast: ToastService
	) {

	}	
	
  	validateFormMessage(form){
  		let response = {
			success: true,
			errorMessage: ''
		};
  		if(!form.valid){
  			response.success = false;	        
	        let separator = '';
	        if (!form.controls.description.valid) {
	          response.errorMessage = 'Ingresa un mensaje';          
	          separator = ', ';
	        }
	        return response; 	        
	    }
	    return response;
  	}

  	messageValidations(form)
  	{	
  		let response = {
			success: true,
			errorMessage: ''
		};
  		
	    response = this.validateFormMessage(form);
	    console.log(response);
	    if(!response.success){        
	        this.toast.show(response.errorMessage);         
	        return response;
	    }
	    return response;
  	}

  	sendMessage(item, message, to = {}){  		
  		console.log(item);
  		let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
	    loading.present();

	    this.messagerest.sendMessage(item, message, to).then(ref =>{
	  		loading.dismiss();
	  		this.toast.show('Enviado!');
	      this.events.publish('user:messages', item, message, Date.now());
	      }).catch((ref) =>{	
	        loading.dismiss();      
	        console.log(ref.error.code);
	        if (ref.error.code == 1002 || ref.error.code==2002) {
	          this.toast.show('Tu sesión expiró, logueate de nuevo!');
	          this.events.publish('user:session', {}, Date.now());
	        } else {
	          this.toast.show('Error, intenta nuevamente!');
	        }		    
	   	});
  	}
}
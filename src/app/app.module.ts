import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { HelpPage } from '../pages/help/help';
import { LoginPage } from '../pages/login/login';
import { PublishPage } from '../pages/publish/publish';
import { MyitemsPage } from '../pages/myitems/myitems';
import { MessagelistPage } from '../pages/messagelist/messagelist';

import { ToastService } from './../services/toast/toast.service';
import { ItemService } from './../services/item/item.service';
import { MessageService } from './../services/message/message.service';
import { RestProvider } from '../providers/rest/rest';
import { UserRestProvider } from '../providers/rest/userrest';
import { ItemRestProvider } from '../providers/rest/itemrest';
import { MessageRestProvider } from '../providers/rest/messagerest';
import { UserLocalProvider } from '../providers/localStorage/userlocalstorage';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HelpPage,
    LoginPage,
    PublishPage,
    MyitemsPage,
    MessagelistPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      frontendImages: 'http://localhost/lumen-jwt/storage/app/public/',
      itemsService: 'http://127.0.0.1/lumen-jwt/public',
      usersService: 'http://localhost/lumen-jwt/public',
      messageService: 'http://localhost/lumen-jwt/public'      
    }
  ),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HelpPage,
    LoginPage,
    PublishPage,
    MyitemsPage,
    MessagelistPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ToastService,
    ItemService,
    MessageService,
    RestProvider,
    UserRestProvider,
    ItemRestProvider,
    MessageRestProvider,
    UserLocalProvider
  ]
})
export class AppModule {}

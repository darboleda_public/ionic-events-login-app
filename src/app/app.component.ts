import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { HelpPage } from '../pages/help/help';
import { LoginPage } from '../pages/login/login';
import { PublishPage } from '../pages/publish/publish';
import { MyitemsPage } from '../pages/myitems/myitems';
import { MessagelistPage } from '../pages/messagelist/messagelist';

import { UserLocalProvider } from '../providers/localStorage/userlocalstorage';
import { ToastService } from './../services/toast/toast.service';
import { User } from './../models/user/user.model';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;

  pages: Array<{title: string, component: any}>;
  loginPages: Array<{title: string, component: any}>;
  guestPages: Array<{title: string, component: any}>;
  loggedIn: boolean;
  user: User;  

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public events: Events,
    private userlocalProvider: UserLocalProvider,
    private toast: ToastService,
    private loadingCtrl: LoadingController
   ) {
    this.initializeApp();

    this.loginPages = [
      { title: 'Buscar', component: HomePage },
      { title: 'Publicar', component: PublishPage },
      { title: 'Mis items', component: MyitemsPage },
      { title: 'Mis mensajes', component: MessagelistPage },      
      { title: 'Ayuda', component: HelpPage }
    ];

    this.guestPages = [
      { title: 'Login/Registro', component: LoginPage },
      { title: 'Buscar', component: HomePage },
      { title: 'Ayuda', component: HelpPage }
    ];

    this.setLogoutState({});
    this.userlocalProvider.getUser().then((val) => {
      console.log(val);
        if (val.token) { 
          this.setLoginState(val);
        }         
     }).catch((ref) =>{        
        console.log('');
     });

    events.subscribe('user:session', (user, time) => {      
      console.log('Welcome', user, 'at', time);
      if (user.token) {
        this.setLoginState(user);
        this.nav.setRoot(HomePage);
      } else {
        this.setLogoutState(user);
        this.nav.setRoot(LoginPage);
      }
    });

    events.subscribe('user:items', (item, action, time) => {      
      console.log(item,' action: ', action, 'at: ', time);      
      this.nav.setRoot(HomePage);
    });

    events.subscribe('user:messages', (item, message, time) => {      
      console.log(item,' message: ', message, 'at: ', time);      
      this.nav.setRoot(MessagelistPage);
    });
  }

  setLoginState(user){
    this.loggedIn = true;
    this.pages = this.loginPages;
    this.user = user;
  }

  setLogoutState(user) {
    this.loggedIn = false;
    this.pages = this.guestPages;
    this.user = user;
  }


  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout(){
    let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
    loading.present();

    this.userlocalProvider.logout().then(ref =>{        
        loading.dismiss();
        this.toast.show('Logout');
        this.events.publish('user:session', {}, Date.now());
      }).catch((ref) =>{        
        console.log('');
        loading.dismiss();
      });    
  }
}


export interface User {
	key?: string;
	email: string;
	password: string;
	name: string;
	token: string;
	id: string;
}
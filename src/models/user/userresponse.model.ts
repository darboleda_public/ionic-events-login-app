export interface UserResponse {	
	token: string;
	user_data: {
		name: string;
		email: string;
		id: string;
	};
}
export interface Item {
	key?: string;
	title: string;
	description: string;
	expireDate: string;
	category: string;
	isForHumans: boolean;
	id: string;
}
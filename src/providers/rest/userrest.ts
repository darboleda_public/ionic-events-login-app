import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './../../models/user/user.model';
import { Config } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserRestProvider {
  	private apiUrl;

  	private httpOptions = {
      headers: new HttpHeaders({        
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT'        
      })
    };
  
	constructor(
		public http: HttpClient,
		public config:Config
	) {
		this.apiUrl = this.config.get('usersService');
	}

  	login(user: User) {  		
	  return new Promise((resolve, reject) => {
	  	const postData = new FormData();
	    postData.append('email', user.email);
	    postData.append('password', user.password);

	    this.http.post(this.apiUrl+'/auth/login', postData, this.httpOptions)
	      .subscribe(res => {
	        resolve(res);
	      }, (err) => {
	        reject(err);
	      });
	  });
	}

	register(user: User) {  		
	  return new Promise((resolve, reject) => {
	  	const postData = new FormData();
	    postData.append('email', user.email);
	    postData.append('password', user.password);
	    postData.append('name', user.name);

	    this.http.post(this.apiUrl+'/auth/register', postData, this.httpOptions)
	      .subscribe(res => {
	        resolve(res);
	      }, (err) => {
	        reject(err);
	      });
	  });
	}

	addUser(data) {
	  return new Promise((resolve, reject) => {
	    this.http.post(this.apiUrl+'/users', JSON.stringify(data), {
		    headers: new HttpHeaders().set('Authorization', 'my-auth-token'),
		    params: new HttpParams().set('param', '1'),
		  })
	      .subscribe(res => {
	        resolve(res);
	      }, (err) => {
	        reject(err);
	      });
	  });
	}

	updateUser(data) {
	  return new Promise((resolve, reject) => {	    
    	this.http.put(this.apiUrl+'/users/1', JSON.stringify(data), {
		    headers: new HttpHeaders().set('Authorization', 'my-auth-token'),
		    params: new HttpParams().set('param', '1'),
		  })
	      .subscribe(res => {
	        resolve(res);
	      }, (err) => {
	        reject(err);
	      });
	  });
	}

	deleteUser(data) {
	  return new Promise((resolve, reject) => {
	    this.http.delete(this.apiUrl+'/users/1', {
		    headers: new HttpHeaders().set('Authorization', 'my-auth-token'),
		    params: new HttpParams().set('param', '1'),
		  })
	      .subscribe(res => {
	        resolve(res);
	      }, (err) => {
	        reject(err);
	      });
	  });
	}
}

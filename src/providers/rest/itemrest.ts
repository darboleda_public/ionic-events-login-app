import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserLocalProvider } from '../../providers/localStorage/userlocalstorage';
import { Item } from '../../models/item/item.model';
import { Config } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ItemRestProvider {
  	private apiUrl;
  
	constructor(
		public http: HttpClient,
		private userlocalProvider: UserLocalProvider,
		public config:Config
	) {
		this.apiUrl = this.config.get('itemsService');
	}
	
	sendItem(url, file, item: Item){
		return new Promise((resolve, reject) => {
		  	const uploadData = new FormData();	  	
		  	let me = this;
		    
		    if (file!=undefined) {
		    	uploadData.append('image', file, file.name);
		    }

		    if (url == 'edititems') {
		    	uploadData.append('id', item.id);
		    }
		    	
		    uploadData.append('title', item.title);
		    uploadData.append('description', item.description);

		    this.userlocalProvider.getUser().then((val) => {
		        if (val.token) {
		        	let httpOptions = {
				      headers: new HttpHeaders({        
				        'Access-Control-Allow-Origin': '*',
				        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
				        'Authorization': 'Bearer '+ val.token
				      })
				    };

				    me.http.post(me.apiUrl+'/'+url, uploadData, httpOptions)
					.subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
					});
		        }         
	     	}).catch((ref) =>{		    
	     	});			    
    	});	 
	}

	getItems(prefix='', searchitem) {

		let httpOptions = { };
		let me = this;
		let params;
		let headers;
		return new Promise((resolve, reject) => {
			this.userlocalProvider.getUser().then((val) => {		      	
		        if (val.token) {		        	
				      headers = new HttpHeaders({        
				        'Access-Control-Allow-Origin': '*',
				        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
				        'Authorization': 'Bearer '+ val.token
				      });
		        }

		        if (searchitem.text) {
		        	params = new HttpParams().set('text', searchitem.text);
		        }

		        httpOptions = {
		        	headers: headers,
		        	params: params
		        };	        

		        me.http.get(this.apiUrl+'/'+prefix+'items', httpOptions)
				.subscribe(res => {
					resolve(res);
				}, (err) => {
					reject(err);
				});         
	     	}).catch((ref) =>{		    
	     	});
	    });  		
	}	
}

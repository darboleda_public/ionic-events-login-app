import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserLocalProvider } from '../../providers/localStorage/userlocalstorage';
import { Message } from '../../models/message/message.model';
import { Config } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessageRestProvider {
  	private apiUrl;
  
	constructor(
		public http: HttpClient,
		private userlocalProvider: UserLocalProvider,
		public config:Config
	) {
		this.apiUrl = this.config.get('messageService');
	}
	
	sendMessage(item, message: Message, to){
		console.log(item);
		return new Promise((resolve, reject) => {
		  	const postData = new FormData();	  	
		  	let me = this;		    
		    	
		    postData.append('items_id', item.id);
		    postData.append('users_id_to', item.user.id);
		    if (to.id) {
		    	postData.append('users_id_to', to.id);
		    }
		    postData.append('message', message.description);
		    if (item.parent_id) {
		    	postData.append('parent_id', item.parent_id);
		    }

		    this.userlocalProvider.getUser().then((val) => {
		        if (val.token) {
		        	let httpOptions = {
				      headers: new HttpHeaders({        
				        'Access-Control-Allow-Origin': '*',
				        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
				        'Authorization': 'Bearer '+ val.token
				      })
				    };

				    me.http.post(me.apiUrl+'/messages', postData, httpOptions)
					.subscribe(res => {
						resolve(res);
					}, (err) => {
						reject(err);
					});
		        }         
	     	}).catch((ref) =>{		    
	     	});			    
    	});	 
	}

	getMessages(suffix='', searchitem) {

		let httpOptions = { };
		let me = this;
		let params;
		let headers;
		return new Promise((resolve, reject) => {
			this.userlocalProvider.getUser().then((val) => {		      	
		        if (val.token) {		        	
				      headers = new HttpHeaders({        
				        'Access-Control-Allow-Origin': '*',
				        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
				        'Authorization': 'Bearer '+ val.token
				      });
		        }

		        if (searchitem.text) {
		        	params = new HttpParams().set('text', searchitem.text);
		        }

		        if (searchitem.items_id) {
		        	params = new HttpParams().set('items_id', searchitem.items_id);
		        }

		        if (searchitem.parent_id) {
		        	params = new HttpParams().set('parent_id', searchitem.parent_id);
		        }

		        httpOptions = {
		        	headers: headers,
		        	params: params
		        };	        

		        me.http.get(this.apiUrl+'/messages'+suffix, httpOptions)
				.subscribe(res => {
					resolve(res);
				}, (err) => {
					reject(err);
				});         
	     	}).catch((ref) =>{		    
	     	});
	    });  		
	}	
}

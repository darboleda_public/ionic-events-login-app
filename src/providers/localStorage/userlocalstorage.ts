import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { User } from './../../models/user/user.model';

@Injectable()
export class UserLocalProvider {

	private table = 'user-data';

	constructor(private db: Storage){

	}

	getUser() {
		return this.db.get(this.table);
	}

	addUser(user: User) {		
        return this.db.set(this.table, user);		
	}

	editUser(user: User) {					
		this.addUser(user);
	}

	remove() {		
		return this.db.set(this.table, {});
	}

	logout() {
		return this.remove();
	}
}
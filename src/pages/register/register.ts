import { Component } from '@angular/core';
import { Events, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { User } from './../../models/user/user.model';
import { UserResponse } from './../../models/user/userresponse.model';
import { UserRestProvider } from '../../providers/rest/userrest';
import { UserLocalProvider } from '../../providers/localStorage/userlocalstorage';
import { ToastService } from './../../services/toast/toast.service';
import { Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

	user: User = {
    name: '',
    email: '',
    password: '',
    token: '',
    id: ''
  };

  form: any;
  submitAttempt: boolean;
  errorMessage: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userProvider: UserRestProvider,
    private userlocalProvider: UserLocalProvider,
    private toast: ToastService,
    private loadingCtrl: LoadingController,
    public events: Events,
    public formBuilder: FormBuilder
  ) {
    let emailRegex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    this.form = this.formBuilder.group({
        name: ['', Validators.compose([Validators.required])],
        email: ['', Validators.compose([Validators.required, Validators.pattern(emailRegex)])],
        password: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  register(user: User){

    this.submitAttempt = true;
    this.errorMessage = '';

    if(!this.form.valid){
        let message = '';
        let separator = '';

        if (!this.form.controls.name.valid) {
          message = 'Nick es obligatoria';          
          separator = ', ';
        }
        if (!this.form.controls.email.valid) {
          message = 'Ingresa un correo válido';          
          separator = ', ';
        }
        if (!this.form.controls.password.valid) {
          message = message + separator + 'clave es obligatoria';
        }
        this.toast.show(message);         
        return false;
    }

    let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
    loading.present();

    this.userProvider.register(user).then((ref: UserResponse) =>{
      loading.dismiss();
      console.log(ref.token);

      let userLocal: User = {
        name: ref.user_data.name,
        email: ref.user_data.email,
        password: '',
        id: ref.user_data.id,
        token: ref.token
      };

      this.toast.show(`${user.email} logged in!`);
      this.userlocalProvider.addUser(userLocal).then(ref =>{        
        this.events.publish('user:session', userLocal, Date.now());
      }).catch((ref) =>{        
        console.log('');
      });
      this.toast.show(`${user.email} registrado!`);
    }).catch((ref) =>{
      loading.dismiss();
      this.errorMessage = ref.error.error;      
      this.toast.show(this.errorMessage);
      console.log(this.errorMessage);
    });
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageaddPage } from './messageadd';

@NgModule({
  declarations: [
    MessageaddPage,
  ],
  imports: [
    IonicPageModule.forChild(MessageaddPage),
  ],
})
export class MessageaddPageModule {}

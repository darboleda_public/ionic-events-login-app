import { Component } from '@angular/core';
import { Events, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Message } from './../../models/message/message.model';
import { Item } from './../../models/item/item.model';
import { Validators, FormBuilder } from '@angular/forms';
import { MessageService } from './../../services/message/message.service';
import { UserLocalProvider } from '../../providers/localStorage/userlocalstorage';
import { ToastService } from './../../services/toast/toast.service';

/**
 * Generated class for the MessageaddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messageadd',
  templateUrl: 'messageadd.html',
})
export class MessageaddPage {

	form: any;	
  to: any;
	submitAttempt: boolean;
	errorMessage: string;
	message: Message = {
  	    description: ''
	};
  item: Item;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	private messageService: MessageService,
  	public formBuilder: FormBuilder,
    private userlocalProvider: UserLocalProvider,
    private toast: ToastService,
    public events: Events
  ) {
  	this.form = this.formBuilder.group({
        description: ['', Validators.compose([Validators.required])]
    });

     this.userlocalProvider.getUser().then((val) => {      
        if (!val.token) { 
          this.toast.show('Tu sesión expiró, logueate de nuevo!');
          this.events.publish('user:session', {}, Date.now());
        }         
     }).catch((ref) =>{        
        this.toast.show('Tu sesión expiró, logueate de nuevo!');
        this.events.publish('user:session', {}, Date.now());
     });
  }

  sendMessage(message: Message) {
  	this.submitAttempt = true;

    let response = this.messageService.messageValidations(this.form);    
    this.errorMessage = response.errorMessage;
    if (!response.success){            
      return false;
    }

    this.messageService.sendMessage(this.item, message, this.to);	  
  }

  ionViewWillLoad() {
    this.item = this.navParams.get('item');
    this.to = this.navParams.get('to');
    console.log(this.item);
    console.log(this.to);
  }
}

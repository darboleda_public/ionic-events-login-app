import { Component } from '@angular/core';
import { Events, Config, LoadingController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from './../../services/toast/toast.service';
import { MessageRestProvider } from '../../providers/rest/messagerest';
import { ResponseMessages } from './../../models/response/response.messages';
import { SearchItem } from './../../models/search/search.item';

/**
 * Generated class for the MessagelistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messagelist',
  templateUrl: 'messagelist.html',
})
export class MessagelistPage {
	messageList:  any;
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	private messagerest: MessageRestProvider,
    private toast: ToastService,
    private loadingCtrl: LoadingController,
    public config:Config,
    public events: Events
  ) {
  	this.loadMessages({});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagelistPage');
  }

  loadMessages(search){
    let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
      loading.present();
      this.messagerest.getMessages('resume',search).then((res: ResponseMessages) => {
        loading.dismiss();
        if (res.http_code == 200) {
          this.messageList = res.messages;
        }        
      }).catch((ref) =>{  
        loading.dismiss();
        console.log(ref.error.code);
        if (ref.error.code == 1002 || ref.error.code==2002) {
          this.toast.show('Tu sesión expiró, logueate de nuevo!');
          this.events.publish('user:session', {}, Date.now());
        } else {
          this.toast.show('Error, intenta nuevamente!');
        }
     });
  }

}

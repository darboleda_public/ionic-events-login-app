import { Component } from '@angular/core';
import { Config, LoadingController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from './../../services/toast/toast.service';
import { ItemRestProvider } from '../../providers/rest/itemrest';
import { ResponseItems } from './../../models/response/response.items';
import { SearchItem } from './../../models/search/search.item';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	itemList:  any;
  searchitem: SearchItem = {
      text: ''
  };
  prefix: string = '';
  pagePush: string = 'ItemdetailPage';
  
  constructor(
  	public navCtrl: NavController,
    public navParams: NavParams,
  	private itemrest: ItemRestProvider,
    private toast: ToastService,
    private loadingCtrl: LoadingController,
    public config:Config
  ) {
     this.loadItems({});
  }

  loadItems(search){
    let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
      loading.present();
      this.itemrest.getItems(this.prefix, search).then((res: ResponseItems) => {
        loading.dismiss();
        if (res.http_code == 200) {
          this.itemList = res.items;
        }        
      }).catch((ref) =>{  
        loading.dismiss();      
        this.toast.show('Error, intenta nuevamente!');
     });
  }
}

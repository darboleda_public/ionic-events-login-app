import { Component } from '@angular/core';
import { Events, Config, LoadingController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from './../../services/toast/toast.service';
import { ItemRestProvider } from '../../providers/rest/itemrest';
import { ResponseItems } from './../../models/response/response.items';
import { SearchItem } from './../../models/search/search.item';
/**
 * Generated class for the MyitemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myitems',
  templateUrl: '../home/home.html',
})
export class MyitemsPage { /*Inheritance between pages it's not safe in ionic for now 08-jul-2018*/
	itemList:  any;  
  searchitem: SearchItem = {
      text: ''
  };
  prefix: string = 'my';
  pagePush: string = 'ItemeditPage';
	
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private itemrest: ItemRestProvider,
		private toast: ToastService,
		private loadingCtrl: LoadingController,
    public config:Config,
    public events: Events,
	) {	
	 this.loadItems({});
	}

  loadItems(search){
    let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
      loading.present();
      this.itemrest.getItems(this.prefix, search).then((res: ResponseItems) => {
        loading.dismiss();
        if (res.http_code == 200) {
          this.itemList = res.items;
        }        
      }).catch((ref) =>{  
        loading.dismiss();
        console.log(ref.error.code);
        if (ref.error.code == 1002 || ref.error.code==2002) {
          this.toast.show('Tu sesión expiró, logueate de nuevo!');
          this.events.publish('user:session', {}, Date.now());
        } else {
          this.toast.show('Error, intenta nuevamente!');
        }
     });
  }
}

import { Component } from '@angular/core';
import { Config, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Item } from './../../models/item/item.model';

/**
 * Generated class for the ItemdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-itemdetail',
  templateUrl: 'itemdetail.html',
})
export class ItemdetailPage {
	item: Item;
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	public config:Config
  ) {
  }

  ionViewWillLoad() {
    this.item = this.navParams.get('item');
  }

}

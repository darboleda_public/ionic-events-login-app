import { Component } from '@angular/core';
import { Events, LoadingController, Config, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastService } from './../../services/toast/toast.service';
import { MessageRestProvider } from '../../providers/rest/messagerest';
import { ResponseMessages } from './../../models/response/response.messages';
import { MessageService } from './../../services/message/message.service';
import { Message } from './../../models/message/message.model';
import { Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the MessagedetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messagedetail',
  templateUrl: 'messagedetail.html',
})
export class MessagedetailPage {

	message: any;
	messageList:  any;
  item: any;
  to: any;
  submitAttempt: boolean;
  form: any;
  errorMessage: string;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public config:Config,
  	private messagerest: MessageRestProvider,
    private toast: ToastService,
    private loadingCtrl: LoadingController,
    public events: Events,
    private messageService: MessageService,
    public formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
        description: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewWillLoad() {
    this.message = this.navParams.get('message');
    this.item = {
        id: this.message.item_id,
        parent_id: this.message.parent_id, 
        user:{ 
          id: this.message.from_id
        }
     };
     this.to = {
       id: this.message.from_id
     };

    this.loadMessages({items_id:this.message.item_id, parent_id:this.message.parent_id});
    console.log(this.message);
  }

  loadMessages(search){
    let loading = this.loadingCtrl.create({content:'Please wait, procesing....'});    
      loading.present();
      this.messagerest.getMessages('',search).then((res: ResponseMessages) => {
        loading.dismiss();
        if (res.http_code == 200) {
          this.messageList = res.messages;
        }        
      }).catch((ref) =>{  
        loading.dismiss();
        console.log(ref.error.code);
        if (ref.error.code == 1002 || ref.error.code==2002) {
          this.toast.show('Tu sesión expiró, logueate de nuevo!');
          this.events.publish('user:session', {}, Date.now());
        } else {
          this.toast.show('Error, intenta nuevamente!');
        }
     });
  }

  sendMessage(message: Message) {
    this.submitAttempt = true;

    let response = this.messageService.messageValidations(this.form);    
    this.errorMessage = response.errorMessage;
    if (!response.success){            
      return false;
    }

    this.messageService.sendMessage(this.item, message, this.to);    
  }
}

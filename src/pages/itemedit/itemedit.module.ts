import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemeditPage } from './itemedit';

@NgModule({
  declarations: [
    ItemeditPage,
  ],
  imports: [
    IonicPageModule.forChild(ItemeditPage),
  ],
})
export class ItemeditPageModule {}

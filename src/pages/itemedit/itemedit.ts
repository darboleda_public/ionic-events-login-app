import { Component } from '@angular/core';
import { Config, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemService } from './../../services/item/item.service';
import { Item } from './../../models/item/item.model';
import { Validators, FormBuilder } from '@angular/forms';

/**
 * Generated class for the ItemeditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-itemedit',
  templateUrl: 'itemedit.html',
})
export class ItemeditPage {
	item: Item;
	form: any;
	selectedFile: File;
	submitAttempt: boolean;
	errorMessage: string;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
    private itemService: ItemService,
    public formBuilder: FormBuilder,    
    public config:Config
  ) {  	
  	this.form = this.formBuilder.group({
        title: ['', Validators.compose([Validators.required])],
        description: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewWillLoad() {
    this.item = this.navParams.get('item');
  }

  onFileChanged(event) {
    let imageValidate = this.itemService.validateImage(<File>event.target.files[0]);    
    this.errorMessage = imageValidate.errorMessage;
    if (!imageValidate.success){      
      return false;
    }
    this.selectedFile = <File>event.target.files[0];    
  }
  
  onUpload(item: Item) {
  	this.submitAttempt = true;
    
    let response = this.itemService.itemValidations(this.selectedFile, this.form);    
    this.errorMessage = response.errorMessage;
    if (!response.success){            
      return false;
    }

    this.itemService.sendItem('edititems', this.selectedFile, item);	
  }
}

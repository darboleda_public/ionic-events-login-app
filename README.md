- Ionic with register/login/logout dynamic side menu.
- CRUD items via REST.
- Session handling in local storage.
- Publish/subscribe events with topics.

If you want a backend user management for this app, you can use this: 
https://gitlab.com/darboleda_public/lumen-JWT-login

1. npm install.

To test local:
- ionic lab

To generate new page with lazy loading:
- ionic generate page PageName.
